---
layout: markdown_page
title: "Community Writers Program"
description: "Start today and earn up to $200 an article."
twitter_image: /images/handbook/marketing/community-writers-twitter-illustration.png
---

<br>

![Write for GitLab](/images/handbook/marketing/community-writers-twitter-illustration.png)

{::options parse_block_html="true" /}

<div class="alert alert-purple center">
## <i class="fa fa-gitlab fa-fw" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i> Get paid to write for GitLab <i class="fa fa-gitlab fa-fw" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>
{:.gitlab-orange .text-center #write-for-gitlab}
</div>

We want to publish you! **Earn up to $200 per article** writing for the GitLab blog.

Are you great at solving challenging problems or explaining complex ideas? Do you
have a specific area of expertise and want to share your knowledge? Help us expand 
our range of tutorials and advice about creating, collaborating, and deploying with 
Git and GitLab.  You don't have to be an experienced writer, we will work with you
to revise, refine, and publish your post to share with our community. 

## How It Works

1. Choose a topic
2. Start writing
3. Get paid

All of GitLab's blog posts begin as an issue in the [GitLab blog post project][blog-project]. 
To become a community author, all you have to do is assign yourself to an open 
issue [labeled "community post" and "up-for-grabs][avail-posts]," write a quick introduction in 
the comments section explaining why you want to write the post, and ping Marcia (@marcia).

If you don't see a topic that interests you, you can propose a new topic by 
creating a new issue and following the instructions above. Once approved, start writing! 

## Get Paid

- In-depth tutorials or opinion pieces of 750+ words: **USD 200.00**
- Short tutorials of 500-750 words: **USD 100.00**
- [Top-priority](#top-priority): **USD 200.00**

<i class="fa fa-exclamation-triangle" aria-hidden="true" style="color: rgb(49, 112, 143);"></i>&nbsp;
Issues labeled **TOP PRIORITY**, will be compensated at **USD 200** regardless of length.
{:.alert .alert-info .text-center #top-priority}

When your post gets published, send us an invoice. GitLab will pay you in 
American Dollars (USD) from a bank account in the USA, via wired transfer 
to your bank account. 

<!-- identifiers -->

[avail-posts]: https://gitlab.com/gitlab-com/blog-posts/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=Community+Posts&label_name%5B%5D=up-for-grabs
[blog-project]: https://gitlab.com/gitlab-com/blog-posts
[CI/CD/CD]: /2016/08/05/continuous-integration-delivery-and-deployment-with-gitlab/
[ConvDev]: /2016/09/13/gitlab-master-plan/#convdev
[GitLab Blog]: /handbook/marketing/blog/#publishing-process-for-community-writers
[our blog]: /blog/
[Pages group]: https://gitlab.com/groups/pages
[Technical Writing]: /handbook/marketing/developer-relations/technical-writing/#professional-writing-techniques
[topics-issues]: https://gitlab.com/gitlab-com/blog-posts/issues/

<!-- labels -->

[Community Posts]: https://gitlab.com/gitlab-com/blog-posts/issues?label_name%5B%5D=Community+Posts
[Up-for-grabs]: https://gitlab.com/gitlab-com/blog-posts/issues?label_name%5B%5D=up-for-grabs
[$100]: https://gitlab.com/gitlab-com/blog-posts/issues?label_name%5B%5D=%24+100
[$200]: https://gitlab.com/gitlab-com/blog-posts/issues?label_name%5B%5D=%24200
[TOP PRIORITY]: https://gitlab.com/gitlab-com/blog-posts/issues?label_name%5B%5D=TOP+PRIORITY

<style>
.center {
  text-align: center;
  display: block;
  margin-right: auto;
  margin-left: auto;
}
.alert-purple {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: rgba(107,79,187,.5);
}
.alert-purple h2 {
      margin-top: 15px;
}
</style>
