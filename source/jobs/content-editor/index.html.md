---
layout: job_page
title: "Content Editor"
---


## Responsibilities  

* Pitching and writing original blog posts:
  * Ideas would be based on community feedback, analytics, gap analysis, input from the sales team, new product     launches and wider industry trends
  * Would aim to increase publishing up to 8-10 posts per week
  * Time permitting, tasks may include sub-editing other team members' and community blog posts
