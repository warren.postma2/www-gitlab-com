---
title: "GitLab Adoption Growing at CERN"
author: Emily von Hoffmann
author_twitter: emvonhoffmann
categories: user stories
image_title: '/images/default-blog-image.png'
description: CERN, the European Organization for Nuclear Research, is increasingly using GitLab to host software projects and code for configuration management.
---

CERN, the European Organization for Nuclear Research, is the world's largest particle physics lab. They are well known for creating [the world’s first website](http://home.cern/topics/birth-web), hosted on Tim Berners-Lee’s computer. 

[GitLab](http://about.gitlab.com) is one of the platforms CERN uses to host software projects and code for configuration management. CERN has about 2500 active developers using GitLab. While most GitLab users at CERN are located in France and Switzerland around Geneva, they also have a community of scientists working from their home institutes in their various member states.

<!-- more -->

Key projects include the software frameworks and libraries for the experiments conducted at CERN. Some of them are hosted on CERN's GitLab instance. Public projects in this category include for instance [Gaudi](https://gitlab.cern.ch/gaudi/Gaudi).They are also using GitLab more and more for Continuous Integration tasks.

### Why Did CERN Choose GitLab?

Alexandre Lossent, Service Manager for the Version Control Systems at CERN, explained that part of their user community has a preference for open-source software; GitLab was also cheaper than other alternatives. 

The most significant benefit is probably how easy GitLab makes it to create a project and start working. Lossent said, “We had more than 2000 projects in a few months, a very fast adoption rate compared to our other code hosting platforms.” 

He has also found that the internal adoption of GitLab is spreading throughout CERN.  He said “While we are not yet actively asking users to move their projects to GitLab, many are attracted by GitLab's features and move proactively.”

Not only are they an active team of users, they have also offered contributions. Lossent contributed the SAML code they wrote for their own usage, and earned an [MVP recognition](https://about.gitlab.com/mvp/) in June 2015. Thanks to their effort, GitLab can be configured to act as a SAML 2.0 Service Provider, [since release 7.12](https://about.gitlab.com/2015/06/22/gitlab-7-12-released/). For this reason, the advantage of an open source code base management tool is clear. 


_Tweet [@GitLab](https://twitter.com/gitlab) and check out our [job openings](https://about.gitlab.com/jobs/)._ 
